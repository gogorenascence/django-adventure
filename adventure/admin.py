from django.contrib import admin
from adventure.models import Room

@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )
