from django.urls import path
from adventure.views import display_room

urlpatterns = [
    path("display_room/<str:room_name>", display_room, name='display_room')
]
