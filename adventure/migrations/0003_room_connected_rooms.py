# Generated by Django 4.1.7 on 2023-02-27 20:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adventure', '0002_room_subtitle'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='connected_rooms',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
