from django.db import models
from django.db.models import JSONField


class Room(models.Model):
    title = models.CharField(max_length=200)
    room_name = models.CharField(max_length=200,null=True)
    subtitle = models.CharField(max_length=200,null=True)
    title_color = models.CharField(max_length=20)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    connected_rooms = JSONField(default=list, blank=True, null=True)
    hidden_word = models.CharField(max_length=200,null=True)


    def __str__(self):
        return self.room_name

    def save(self, *args, **kwargs):
        # If the title field is not set, set it to the ID
        if not self.room_name:
           self.room_name  = str(self.id)
        super(Room, self).save(*args, **kwargs)
